import java.lang.reflect.Method;

/**
 * @projectname: java常用代码片段
 * @blog: yangshengliang.com
 * @author: fedkey
 * @Date: 2018/1/29
 * @Description:在windows /linux /mac 上调用浏览器打开
 */
public class WebBrowser {
    public static void vistUrl(String url) {
        final String OS_WINDOWS = "windows";
        final String OS_LINUX = "linux";
        final String OS_MAC = "mac os";
        String osName = System.getProperty("os.name");
        if(osName != null){
            osName = osName.toLowerCase();
        }
        try {
            if (osName.startsWith(OS_MAC)) {
                Class fileMgr = Class.forName("com.apple.eio.FileManager");
                Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[]{String.class});
                openURL.invoke(null, new Object[]{url});

            } else if (osName.startsWith(OS_WINDOWS)) {
                Runtime.getRuntime().exec(
                        "rundll32 url.dll,FileProtocolHandler " + url);

            } else if (osName.contains(OS_LINUX)) {
                String[] browsers = {"chromium", "firefox", "google-chrome-stable", "google-chrome-dev", "opera", "konqueror", "epiphany",
                        "mozilla", "netscape"
                };
                String browser = null;
                for (int count = 0; count < browsers.length && browser == null; count++) {
                    if (Runtime.getRuntime().exec(new String[]{
                            "which", browsers[count]}).waitFor() == 0) {
                        browser = browsers[count];
                    }
                }
                if (browser == null) {
                    throw new Exception("Could not find web browser");

                } else {
                    Runtime.getRuntime().exec(new String[]{browser, url});
                }

            }
        } catch (Exception io) {
            io.printStackTrace();
        }
    }
}